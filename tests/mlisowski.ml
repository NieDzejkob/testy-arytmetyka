open Arytmetyka;;

let epsilon = 0.000001;;

let is_nan x =
    match classify_float x with
    | FP_nan -> true
    | _ -> false;;

let is_inf x = x = infinity;;

let is_neg_inf x = x = neg_infinity;;

let is_eq x y = abs_float (x -. y) < epsilon;;

(* sprawdzenie poprawności konstruktorów i selektorów *)

let a = wartosc_dokladnosc 135. 15.;;
let b = wartosc_dokladnosc (-14.) 170.;;
let c = wartosc_od_do (-1.) 4.;;
let d = wartosc_dokladna 6.5;;

assert (is_eq (min_wartosc a) 114.75);;
assert (is_eq (max_wartosc a) 155.25);;
assert (is_eq (sr_wartosc a) 135.);;
assert (is_eq (min_wartosc b) (-37.8));;
assert (is_eq (max_wartosc b) 9.8);;
assert (is_eq (sr_wartosc b) (-14.));;
assert (is_eq (min_wartosc c) (-1.));;
assert (is_eq (max_wartosc c) 4.);;
assert (is_eq (sr_wartosc c) 1.5);;
assert (is_eq (min_wartosc d) 6.5);;
assert (is_eq (max_wartosc d) 6.5);;
assert (is_eq (sr_wartosc d) 6.5);;

(* sprawdzanie poprawności modyfikatorów *)

let zero = wartosc_dokladna 0.;;
let jeden = wartosc_dokladna 1.;;
let dwa = wartosc_dokladna 2.;;
let neg_trzy = wartosc_dokladna (-3.);;
let a = podzielic jeden zero;;
let b = wartosc_od_do 2.5 3.3;;
let c = wartosc_od_do (-4.5) (-0.1);;
let d = wartosc_od_do (-0.5) 5.0;;
let e = wartosc_od_do 0.0 3.0;;
let f = wartosc_od_do (-2.4) 0.0;;

(* wszystkie tutaj powinny być zbiorem pustym *)
let g = plus a b;;
let h = minus a c;;
let i = razy a d;;
let j = podzielic a e;;
let k = podzielic f a;; 

let l = podzielic dwa c;;           (* <-20, -0.4444...> *)
let m = podzielic l d;;             (* (-inf, -0.0888...> U <0.8888..., +inf) *)
let n = podzielic jeden e;;         (* <0.3333...,+inf) *)
let o = podzielic neg_trzy e;;      (* (-inf, -1> *)
let p = podzielic dwa f;;           (* (-inf, -0.8333...> *)
let q = razy b b;;                  (* <6.25, 10.89> *)
let r = podzielic b b;;             (* <0.7575..., 1.32> *)
let s = razy c d;;                  (* <-22.5, 2.25> *)
let t = razy m zero;;               (* <0, 0> *)
let u = plus n o;;                  (* (-inf, +inf) *)
let v = plus m d;;                  (* (-inf, +inf) *)
let w = plus m b;;                  (* (-inf, 3.2111...> U <3.3888..., +inf) *)
let x = minus b m;;                 (* (-inf, 2.4111...> U <2.5888..., +inf) *)
let y = minus m b;;                 (* (-inf, -2.5888...> U <-2.4111..., +inf) *)
let z = podzielic jeden m;;         (* <-11.25, 1.125> *)
(* nie znam poprawnej kolejności greckiego alfabetu *)
let alfa = razy o p;;               (* <0.8333..., +inf) *)
let beta = razy m s;;               (* (-inf, +inf) *)
let gamma = razy e f;;              (* <-7.2, 0> *)
let delta = plus d zero;;           (* <-0.5, 5> *)
let theta = razy m m;;              (* (-inf, -0.079012345...> U <0.007901234..., +inf) *)
let phi = razy n o;;                (* (-inf, -0.3333..> *)
let ksi = razy o e;;                (* (-inf, 0> *)
let kappa = plus jeden p;;          (* (-inf, 0.1666...> *)
let tau = razy kappa f;;            (* <-0.4, +inf) *)
let omega = razy kappa o;;          (* (-inf, +inf) *)

assert (is_eq (min_wartosc (minus zero (plus jeden dwa))) (-3.));;
assert (is_eq (max_wartosc (minus zero (plus jeden dwa))) (-3.));;
assert (is_nan (min_wartosc g));;
assert (is_nan (max_wartosc h));;
assert (is_nan (min_wartosc i));;
assert (is_nan (max_wartosc j));;
assert (is_nan (min_wartosc k));;
assert (is_eq (min_wartosc l) (-20.));;
assert (is_eq (max_wartosc l) (-0.4444444444444));;
assert (is_neg_inf (min_wartosc m));;
assert (is_inf (max_wartosc m));;
assert (not (in_wartosc m 0.1));;
assert (is_eq (min_wartosc n) 0.3333333333333);;
assert (is_inf (max_wartosc n));;
assert (is_neg_inf (min_wartosc o));;
assert (is_eq (max_wartosc o) (-1.));;
assert (is_neg_inf (min_wartosc p));;
assert (is_eq (max_wartosc p) (-0.83333333333));;
assert (is_eq (min_wartosc q) 6.25);;
assert (is_eq (max_wartosc q) 10.89);;
assert (is_eq (min_wartosc r) 0.757575757575);;
assert (is_eq (max_wartosc r) 1.32);;
assert (is_eq (sr_wartosc s) (-10.125));;
assert (is_eq (min_wartosc t) 0.);;
assert (is_eq (max_wartosc t) 0.);;
assert (is_nan (sr_wartosc u));;
assert (in_wartosc u 1.618033988);;
assert (is_nan (sr_wartosc v));;
assert (in_wartosc v 2.7182818284);;
assert (is_nan (sr_wartosc w));;
assert (not (in_wartosc w 3.3));;
assert (is_nan (sr_wartosc x));;
assert (not (in_wartosc x 2.5));;
assert (is_nan (sr_wartosc y));;
assert (not (in_wartosc y (-2.5)));;
assert (is_eq (min_wartosc z) (-11.25));;
assert (is_eq (max_wartosc z) 1.125);;
assert (is_eq (min_wartosc alfa) 0.83333333333333);;
assert (is_inf (sr_wartosc alfa));;
assert (is_nan (sr_wartosc beta));;
assert (in_wartosc beta 0.423310825);;
assert (is_eq (min_wartosc gamma) (-7.2));;
assert (is_eq (max_wartosc gamma) 0.);;
assert (is_eq (min_wartosc delta) (-0.5));;
assert (is_eq (max_wartosc delta) 5.);;
assert (is_nan (sr_wartosc theta));;
assert (not (in_wartosc theta 0.));;
assert (is_neg_inf (min_wartosc phi));;
assert (is_eq (max_wartosc phi) (-0.33333333333));;
assert (is_neg_inf (min_wartosc ksi));;
assert (is_eq (max_wartosc ksi) 0.);;
assert (is_neg_inf (min_wartosc kappa));;
assert (is_eq (max_wartosc kappa) 0.166666666);;
assert (is_eq (min_wartosc tau) (-0.4));;
assert (is_inf (max_wartosc tau));;
assert (is_neg_inf (min_wartosc omega));;
assert (is_inf (max_wartosc omega));;
assert (is_nan (sr_wartosc omega));;
assert (in_wartosc omega 0.03141592653589);;



