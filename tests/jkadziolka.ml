open Arytmetyka;;

let epsilon = 0.000001;;

let is_nan x =
    match classify_float x with
    | FP_nan -> true
    | _ -> false;;

let is_inf x = x = infinity;;

let is_neg_inf x = x = neg_infinity;;

let is_eq x y = abs_float (x -. y) < epsilon;;

(* podobno da się napisać buga tak, że inne testy tego nie złapią *)

let a = wartosc_od_do 2. 4.;;
let b = wartosc_od_do (-1.) 1.;;
let c = podzielic (wartosc_dokladna 2.) b;;   (* (-inf, -2) U (2, inf) *)
let d = plus (wartosc_dokladna 5.) c;;        (* (-inf, 3) U (7, inf) *)
let e = razy a d;;                            (* (-inf, 12) U (14, inf) *)

assert (is_neg_inf (min_wartosc e));;
assert (is_inf (max_wartosc e));;
assert (in_wartosc e 11.99);
assert (not (in_wartosc e 12.01));
assert (not (in_wartosc e 13.99));
assert (in_wartosc e 14.01);
